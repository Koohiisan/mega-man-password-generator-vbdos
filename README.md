# Mega Man Password Generator #
### Visual Basic 1.0 for DOS version ###

I wrote this in Visual Basic 1.0 for DOS using an MS-DOS/Windows 3.11 VM.  Why?  Because I can, and because I enjoy writing things in Visual Basic.
![cover](https://bitbucket.org/Koohiisan/mega-man-password-generator-vbdos/raw/6a95a8fa9b7c134bc7787627f5fbc3d6c1023cab/img/cover.png)

### Games currently working ###

* Mega Man 2 (NES) / Rockman 2 (FC)

* Mega Man 3 (NES) / Rockman 3 (FC)

* Mega Man 4 (NES) / Rockman 4 (FC)

### Installing ###
Simply copy one of the EXE files from the bin directory of this repository to a place where you can execute it in a DOS environment. 

This *will* run on actual, legacy hardware--even on hardware from before Windows 3.x came along.  Conversely, you can run it in an MS-DOS or FreeDOS Virtual Machine, or in a DOSBox instance.

### Version history ###
v1.3.0

* Modified initial form for better UX

v1.2.0

Mega Man 4 NES / Rockman 4 FC

* Initial release

v1.1.0

Mega Man 2 NES / Rockman 2 FC

* Initial release

v1.0.2

* Added 'about' dialog

v1.0.1

Mega Man 3 NES / Rockman 3 FC

* Improved explanation of original Japanese pun

* Fixed issue with keyboard usage to select/deselect options

v1.0.0

Mega Man 3 NES / Rockman 3 FC

* Initial release